# Native Ads [ALPHA]

**Decentralized ad protocol for the Hive blockchain**

The Native Ads protocol enables Hive dApps to monetize by selling ad space and ad time.

## Development

- Python 3.6 required
- PostgreSQL 10+

**Dependencies:**

- Ubuntu: `sudo apt install python3 python3-pip`

**Installation:**

- Clone the repo
- `cd native-ads`
- `pip3 install -e .`

**Run:**

*Runs sync service for now. Server still WIP*

- `cd native_ads`
- `python3 run.py`