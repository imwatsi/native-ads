import sys

from setuptools import find_packages
from setuptools import setup

assert sys.version_info[0] == 3 and sys.version_info[1] >= 6, "Native Ads requires Python 3.6 or newer"

setup(
    name='native_ads',
    version='0.0.1',
    description='Decentralized ad protocol for the Hive blockchain.',
    long_description=open('README.md').read(),
    packages=find_packages(exclude=['scripts']),
    install_requires=[
        'psycopg2',
        'requests',
        'aiohttp',
        'jsonrpcserver',
        
    ],
    entry_points = {
        'console_scripts': [
            'native_ads = native_ads.run:run'
        ]
    }
)