"""Synchronizes an in-memory cache for accounts."""


class AccountsCache:

    hive_accounts = {}
    accounts = {"monetizer": {}, "advertizer": {}}
    monetizer_map = {}
    advertizer_map = {}
    flagged_monetizer_accs = []
    flagged_advertizer_accs = []

    @classmethod
    def init(cls, db):
        cls.db = db
    
    @classmethod
    def check_acc_db_entry(cls, name, timestamp):
        acc = cls.db.get_hive_account(name)
        if acc is None:
            cls.db.new_hive_account(name, timestamp)
            cls._fetch_hive_acc_from_db(name)

    @classmethod
    def flush(cls):
        if len(cls.flagged_monetizer_accs) > 0:
            for name in cls.flagged_monetizer_accs:
                cls._update_acc('monetizer', name)
        cls.flagged_monetizer_accs.clear()
        if len(cls.flagged_advertizer_accs) > 0:
            for name in cls.flagged_advertizer_accs:
                cls._update_acc('advertizer', name)
        cls.flagged_advertizer_accs.clear()
    
    @classmethod
    def flag_acc(cls, acc_type, name):
        if acc_type == 'monetizer':
            cls.flagged_monetizer_accs.append(name)
        elif acc_type == 'advertizer':
            cls.flagged_advertizer_accs.append(name)
    
    @classmethod
    def is_cached_acc(cls, acc_type, name):
        return name in cls.accounts[acc_type]
    
    @classmethod
    def is_cached_hive_acc(cls, name):
        return name in cls.hive_accounts

    @classmethod
    def _fetch_acc_from_db(cls, acc_type, name):
        if acc_type == 'monetizer':
            db_acc = cls.db.get_monetizer_acc(name)
        elif acc_type == 'advertizer':
            db_acc = cls.db.get_advertizer_acc(name)
        if db_acc:
            if db_acc['name'] == name:
                cls._add_to_cache(acc_type, db_acc)
                return True
            else:
                # sanitiy check, highly unlikely
                raise Exception('invalid DB lookup')
        return False
    
    @classmethod
    def _fetch_hive_acc_from_db(cls, name):
        db_acc = cls.db.get_hive_account(name)
        if db_acc:
            cls._add_hive_acc_to_cache(db_acc)
            return True
        else:
            return False

    @classmethod
    def get_acc_id(cls, acc_type, name):
        if cls.is_cached_acc(acc_type, name):
            return cls.accounts[acc_type][name]['id']
        else:
            return cls.accounts[acc_type][name]['id'] if cls._fetch_acc_from_db(acc_type, name) else None
    
    @classmethod
    def get_hive_acc(cls, name):
        if cls.is_cached_hive_acc(name):
            return cls.hive_accounts[name]
        else:
            return dict(cls.hive_accounts[name]) if cls._fetch_hive_acc_from_db(name) else None
    
    @classmethod
    def get_hive_acc_id(cls, name):
        if cls.is_cached_hive_acc(name):
            return cls.hive_accounts[name]['id']
        else:
            return cls.hive_accounts[name]['id'] if cls._fetch_hive_acc_from_db(name) else None
    
    @classmethod
    def get_account_name(cls, acc_type, acc_id):
        if acc_type == 'monetizer':
            if acc_id in cls.monetizer_map:
                return cls.monetizer_map[acc_id]
            else:
                db_name = cls.db.get_monetizer_name(acc_id)
                if db_name:
                    cls.monetizer_map[acc_id] = db_name
                    return db_name
        elif acc_type == 'advertizer':
            if acc_id in cls.advertizer_map:
                return cls.advertizer_map[acc_id]
            else:
                db_name = cls.db.get_advertizer_name(acc_id)
                if db_name:
                    cls.advertizer_map[acc_id] = db_name
                    return db_name

    @classmethod
    def get_account(cls, acc_type, name):
        if cls.is_cached_acc(acc_type, name):
            return cls.accounts[acc_type][name]
        else:
            return dict(cls.accounts[acc_type][name]) if cls._fetch_acc_from_db(acc_type, name) else None

    @classmethod
    def _add_to_cache(cls, acc_type, details):
        name = details['name']
        del details['name']
        cls.accounts[acc_type][name] = details
        if acc_type == 'monetizer':
            cls.monetizer_map[details['id']] = name
        elif acc_type == 'advertizer':
            cls.advertizer_map[details['id']] = name
    
    @classmethod
    def _add_hive_acc_to_cache(cls, details):
        name = details['name']
        del details['name']
        cls.hive_accounts[name] = details 

    @classmethod
    def _update_acc(cls, acc_type, name):
        if acc_type == 'monetizer':
            db_acc = cls.db.get_monetizer_acc(name)
        elif acc_type == 'advertizer':
            db_acc = cls.db.get_advertizer_acc(name)
        if db_acc:
            if name not in cls.accounts[acc_type]:
                cls.accounts[acc_type][name] = {}
            keys = db_acc.keys()
            for k in keys:
                cls.accounts[acc_type][name][k] = db_acc[k]

    @classmethod
    def update_hive_acc(cls, name):
        cls._fetch_hive_acc_from_db(name)
