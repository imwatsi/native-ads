from datetime import datetime

from native_ads.hive.server_requests import make_request
from native_ads.utils.defaults import NATIVE_POST_TYPES
from native_ads.utils.tools import get_cleaned_dict
from native_ads.cache.accounts import AccountsCache

CLEAN_POST_CONTENT_FIELDS = [
    'title', 'last_update', 'allow_votes'
]
# TODO: add more

class AdPostUpdates:
    flagged_posts = []

    @classmethod
    def flag_post(cls, post_id, author, permlink, ad_type):
        cls.flagged_posts.append(
            {
                'post_id': post_id,
                'author': author,
                'permlink': permlink,
                'ad_type': ad_type
            }
        )
    
    @classmethod
    def flush(cls, db):
        for post in cls.flagged_posts:
            cls._update_post(db, post)
        cls.flagged_posts.clear()
    
    @classmethod
    def _update_post(cls, db, post):
        content = cls.get_post_content(post['author'], post['permlink'])
        if post['ad_type'] in NATIVE_POST_TYPES:
            # TODO: pass cleaned_dict with post_props to AdPostsCache
            pass
        details = {'content': content['body']}
        db.update_ad_post(post['post_id'], details)
    
    @classmethod
    def get_post_content(cls, author, permlink):
        return make_request(
            "condenser_api.get_content",
            [author, permlink]
        )

class AdPostsCache:

    # TODO: implement auto-clean, last_fetched tracking

    posts = {} # id, advertizer_id, ad_type, permlink, content, properties, is_deleted, post_props
    new_ad_posts = []

    @classmethod
    def init(cls, db):
        cls.db = db
    
    @classmethod
    def flush(cls):
        for ad_post in cls.new_ad_posts:
            post_id = cls.get_post_id(ad_post['author'], ad_post['permlink'])
            if ad_post['ad_type'] in NATIVE_POST_TYPES:
                # TODO: add extra fields to post_props
                _post_props = AdPostUpdates.get_post_content(
                    ad_post['author'],
                    ad_post['permlink']
                )
                ad_post['post_props'] = get_cleaned_dict(
                    _post_props,
                    CLEAN_POST_CONTENT_FIELDS,
                    True
                )
            ad_post['id'] = post_id
            cls._add_post_to_cache(ad_post)
        cls.new_ad_posts.clear()
        AdPostUpdates.flush(cls.db)
    
    @classmethod
    def _add_post_to_cache(cls, details):
        # id created permlink ad_type content properties author advertizer_id
        author = details['author']
        del details['author']
        permlink = details['permlink']
        del details['permlink']
        details['cache_timestamp'] = datetime.utcnow()
        details['cache_timestamp_accessed'] = datetime.utcnow()
        if author not in cls.posts:
            cls.posts[author] = {}
        cls.posts[author][permlink] = details
    
    
    @classmethod
    def add_new_post_to_queue(cls, advertizer_id, author, details):
        post = details
        post['advertizer_id'] = advertizer_id
        post['author'] = author
        cls.new_ad_posts.append(post)
    
    @classmethod
    def _fetch_post_from_db(cls, author, permlink):
        advertizer_id = AccountsCache.get_acc_id('advertizer', author)
        assert advertizer_id, "account '{}' has not initialized an advertizer account"
        db_post = cls.db.get_ad_post(advertizer_id, permlink)
        if db_post:
            db_post['author'] = author
            cls._add_post_to_cache(db_post)
            return True
        return False
    
    @classmethod
    def is_cached_post(cls, author, permlink):
        if author in cls.posts:
            if permlink in cls.posts[author]:
                cls.posts[author][permlink]['cache_timestamp_accessed'] = datetime.utcnow()
                return True
        return False
    
    @classmethod
    def get_post(cls, author, permlink):
        if cls.is_cached_post(author, permlink):
            return cls.posts[author][permlink]
        else:
            return dict(cls.posts[author][permlink]) if cls._fetch_post_from_db(author, permlink) else None
    
    @classmethod
    def get_post_id(cls, author, permlink):
        if cls.is_cached_post(author, permlink):
            return cls.posts[author][permlink]['id']
        else:
            return cls.posts[author][permlink]['id'] if cls._fetch_post_from_db(author, permlink) else None
    
    @classmethod
    def get_ad_type(cls, author, permlink):
        if cls.is_cached_post(author, permlink):
            return cls.posts[author][permlink]['ad_type']
        else:
            return cls.posts[author][permlink]['ad_type'] if cls._fetch_post_from_db(author, permlink) else None