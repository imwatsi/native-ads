from datetime import datetime, timedelta

from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache
from native_ads.engine.op_validation import AdvertizerOpValidation
from native_ads.engine.op_history import NativeAdOpHistory
from native_ads.engine.notifications import Notifications
from native_ads.utils.tools import (
    check_required_keys, check_allowed_keys, get_cleaned_dict,
    START_BLOCK, UTC_TIMESTAMP_FORMAT
)
from native_ads.utils.defaults import Status

ALLOWED_KEYS = {
    'account_init': ['profile'],
    'set_props': ['profile'],
    'ad_submit': [
        'monetizer', 'ad_space', 'permlink', 'time_units',
        'start_time', 'bid_amount', 'bid_token'
    ],
    'ad_bid': [
        'monetizer', 'ad_space', 'permlink', 'time_units',
        'start_time', 'bid_amount', 'bid_token'
    ],
    'ad_withdraw': [
        'monetizer', 'ad_space', 'permlink'
    ],
    'ad_fund': [
        'monetizer_id', 'ad_space_id', 'to_acc',
        'advertizer_id', 'ad_id', 'amount', 'token'
    ]
}

REQUIRED_KEYS = {
    'account_init': [],
    'set_props': ['profile'],
    'ad_submit': [
        'monetizer', 'ad_space', 'permlink',
        'time_units', 'bid_amount', 'bid_token'
    ],
    'ad_bid': [
        'monetizer', 'ad_space', 'permlink',
        'bid_amount', 'bid_token'
    ],
    'ad_withdraw': [
        'monetizer', 'ad_space', 'permlink'
    ],
    'ad_fund': [
        'monetizer_id', 'ad_space_id', 'to_acc',
        'advertizer_id', 'ad_id', 'amount', 'token']
}

OP_START = {
    'account_init': START_BLOCK,
    'set_props': START_BLOCK,
    'ad_submit': START_BLOCK,
    'ad_bid': START_BLOCK,
    'ad_withdraw': START_BLOCK,
    'ad_fund': START_BLOCK
}

class AdvertizerOp:

    def __init__(self, db, block_num, timestamp, acc, op_name, op_data):
        self.db = db
        self.block_num = block_num
        self.timestamp = timestamp
        self.acc = acc
        self.op_name = op_name
        self.op_data = op_data
        self.ad_state = None
        self.ads_context = None
        self.override_reject = False
        self.reduced_time_units = False
        self.op_handlers = {
            'account_init': self.account_init,
            'set_props': self.set_props,
            'ad_submit': self.ad_submit,
            'ad_bid': self.ad_bid,
            'ad_withdraw': self.ad_withdraw,
            'ad_fund': self.ad_fund
        }
        try:
            # validation
            if op_name not in OP_START: return
            if self.block_num < OP_START[op_name]: return
            self._validate_op_structure()
            self.process()
        except AssertionError as e:
            Notifications.new_notification(
                self.timestamp, self.acc,
                'advertizer', 'error', str(e)
            )
    
    def process(self):
        self.op_handlers[self.op_name]()

    def account_init(self):
        AdvertizerOpValidation.acc_props(self.op_data)
        _id = AccountsCache.get_acc_id('advertizer', self.acc)
        assert not _id, f"advertizer account for {self.acc} already initialized"
        self.db.new_advertizer_acc(self.acc, self.timestamp, self.op_data)
        AccountsCache.flag_acc('advertizer', self.acc)
        AccountsCache.check_acc_db_entry(self.acc, self.timestamp)

    def set_props(self):
        AdvertizerOpValidation.acc_props(self.op_data)
        _id = AccountsCache.get_acc_id('advertizer', self.acc)
        assert _id is not None, f"advertizer account for {self.acc} already initialized"
        self.db.update_advertizer_acc(_id, self.op_data)
        AccountsCache.flag_acc('advertizer', self.acc)

    def ad_submit(self):
        AdvertizerOpValidation.ad_submit(self.op_data)
        self._id_prefetch_and_validation()
        self._pre_process_checks()
        details = get_cleaned_dict(
            self.op_data,
            ['monetizer', 'ad_space', 'permlink']
        )
        if self.ad_state:
            details['status'] = Status.submitted
            self.db.update_ad_state(self.ad_id, self.space_id, details)
        else:
            details['advertizer_id'] = self.advertizer_id
            details['ad_id'] = self.ad_id
            details['space_id'] = self.space_id
            details['status'] = Status.submitted
            self.db.new_ad_state(details)
    
    def ad_bid(self):
        AdvertizerOpValidation.ad_bid(self.op_data)
        self._id_prefetch_and_validation()
        self._pre_process_checks()
        details = get_cleaned_dict(
            self.op_data,
            ['monetizer', 'ad_space', 'permlink']
        )
        self.db.update_ad_state(self.ad_id, self.space_id, details)
    
    def ad_withdraw(self):
        AdvertizerOpValidation.ad_withdraw(self.op_data)
        self._id_prefetch_and_validation()
        self._pre_process_checks()
        self.db.update_ad_state(self.ad_id, self.space_id, {'status': Status.draft})
    
    def ad_fund(self):
        self.ad_id = self.op_data['ad_id']
        self.space_id = self.op_data['ad_space_id']
        self.monetizer_id = self.op_data['monetizer_id']
        self.advertizer_id = self.op_data['advertizer_id']
        self._pre_process_checks()
        details = {'status': Status.scheduled}
        if self.reduced_time_units:
            details['time_units'] = self.ad_state['time_units']
        if self.override_reject:
            details['mod_notes'] = ''
        self.db.update_ad_state(self.ad_id, self.space_id, details)
        
    def _validate_op_structure(self):
        check_required_keys(
            REQUIRED_KEYS[self.op_name],
            self.op_data.keys(),
            f'{self.op_name} op'
        )
        check_allowed_keys(
            ALLOWED_KEYS[self.op_name],
            self.op_data.keys(),
            f'{self.op_name} op'
        )
    
    def _id_prefetch_and_validation(self):
        if self.op_name == 'ad_fund': return
        # get advertizer ID
        self.advertizer_id = AccountsCache.get_acc_id('advertizer', self.acc)
        assert self.advertizer_id is not None, (
            f"account '{self.acc}' does not have an advertizer account"
        )
        # get monetizer ID
        self.monetizer_id = AccountsCache.get_acc_id('monetizer', self.op_data['monetizer'])
        assert self.monetizer_id is not None, (
            f"account '{self.op_data['monetizer']}' does not have a monetizer account"
        )
        # get space ID
        self.space_id = self.db.get_monetizer_space_id(
            self.monetizer_id, self.op_data['ad_space']
        )
        assert self.space_id is not None, (
            f"invalid ad_space entered; could not find "
            f"(@{self.op_data['monetizer']}/{self.op_data['ad_space']}"
        )
        # get ad ID
        self.ad_id = AdPostsCache.get_post_id(self.acc, self.op_data['permlink'])
        assert self.ad_id is not None, (
            f"invalid ad permlink; could not find "
            f"(@{self.acc}/{self.op_data['permlink']}"
        )
    
    def _pre_process_checks(self):
        """Validate the advertizer op."""

        self._validate_ad_state()

        # load Monetizer's Ads context
        self.ads_context = self.db.get_monetizer_context(self.monetizer_id)

        self._validate_ad_compliance()
    
    def _validate_ad_state(self):
        """Checks the operation against the rules permitted for the ad's current state."""
        action = self.op_name
        
        if action != 'ad_fund':
            valid_ad = AdPostsCache.get_post_id(self.acc, self.op_data['permlink'])
            assert valid_ad, 'the specified post is not a valid ad'

        self.ad_state = self.db.get_ad_state(self.ad_id, self.space_id)

        if self.ad_state:
            ad_status = self.ad_state['status']
        else:
            ad_status = None
        

        if action == 'ad_submit':
            assert ad_status in [None, Status.draft], (
                'can only submit ads that are new or in draft status')

        else:

            assert self.ad_state, (
                'ad not yet submitted to Monetizer; cannot perform %s op' % action)

            if action == 'ad_bid':
                assert ad_status == Status.submitted, 'can only bid for ads that are pending review'
            elif action == 'ad_withdraw':
                assert ad_status in [Status.submitted, Status.approved], (
                    "can only withdraw submitted or approved ads, not '%s' ads"
                    % Status(ad_status).name
                )
            elif action == 'ad_fund':
                conflict_rej = NativeAdOpHistory.check_block_hist(
                    self.block_num,
                    'monetizer',
                    self.monetizer_id,
                    self.advertizer_id,
                    'ad_reject',
                    self.space_id,
                    self.ad_id
                )
                if conflict_rej and ad_status == Status.draft:
                    # override adReject op
                    # TODO: notify mod
                    self.override_reject = True
                else:
                    # proceed with normal validation
                    assert ad_status == Status.approved, (
                        "you have funded an ad with status '%s'; "
                        "consider contacting the monetizer's management "
                        "to resolve this") % Status(ad_status).name

    def _validate_ad_compliance(self):
        """Check if operation complies with monetizer level ad settings"""

        action = self.op_name

        if action not in ['ad_fund', 'ad_withdraw']:  # exempt from accepts_ad check
            accepts_ads = self.ads_context['enabled']
            assert accepts_ads, 'Monetizer does not accept ads'

        if action in ['ad_submit', 'ad_bid']:
            self._check_bid()
            if 'start_time' in self.op_data:
                # check if time is in the future and respects community delay setting
                delay = self.ads_context['scheduled_delay']
                earliest_time = datetime.utcnow() + timedelta(minutes=delay)
                start_time = datetime.strptime(self.op_data['start_time'], UTC_TIMESTAMP_FORMAT)
                assert start_time > earliest_time, (
                    "start_time should be after %s (UTC) according to the community's "
                    "scheduled_delay of %d minutes" %(earliest_time, delay))
            if action == 'ad_submit':
                self._check_space_compliance()

        if action == 'ad_fund':

            # check timeout
            ad_timed_out = self._check_ad_timeout()
            assert not ad_timed_out, (
                "ad payment is late; contact Monetizer management to resolve the issue"
            )

            # check symbol
            expected_token = self.ads_context['token']
            assert self.op_data['token'] == expected_token, (
                'wrong token sent for ad payment; expected %s' % expected_token
            )

            # check payment account
            burn = self.ads_context['burn']
            _to = self.op_data['to_acc']
            if burn:
                assert _to == 'null', (
                    'Monetizer only accepts burn payments for ads; '
                    'contact Monetizer management to resolve the issue')

            # check paid amount
            expected_amount = self.ad_state['bid_amount']
            sent_amount = self.op_data['amount']
            if sent_amount > expected_amount:
                diff = sent_amount - expected_amount
                # TODO: soft notify for refund of difference
            elif sent_amount < expected_amount:
                # recalc time units for under-paid ads
                og_time_units = self.ad_state['time_units']
                og_amount = self.ad_state['bid_amount']
                pptu = og_amount/og_time_units  # price-per-time-unit
                new_time_units = int(sent_amount/pptu)
                # TODO: soft notify
                self.ad_state['time_units'] = new_time_units
                self.reduced_time_units = True

    def _check_space_compliance(self):
        expecting = self.db.get_monetizer_space_ad_type(self.space_id)
        assert expecting, "monetizer_space not found (_check_space_compliance)" # TODO: remove sanity check
        provided = AdPostsCache.get_ad_type(self.acc, self.op_data['permlink'])
        assert provided == expecting, (
            f"ad_space expecting '{expecting}' (ad_type); provided '{provided}''"
        )
    
    def _check_bid(self):
        """Check if bid token, amount and time units respect Monetizer's preferences."""

        if 'bid_amount' in self.op_data:
            op_bid_amount = self.op_data['bid_amount']
        else:
            assert self.ad_state['bid_amount'], 'missing bid_amount for ad'
            op_bid_amount = self.ad_state['bid_amount']
        assert op_bid_amount > 0, 'bid amount must be greater than zero (0)'

        if 'time_units' in self.op_data:
            op_time_units = self.op_data['time_units']
        else:
            assert self.ad_state['time_units'], 'missing time_units for ad'
            op_time_units = self.ad_state['time_units']
        assert op_time_units > 0, 'time units must be greater than zero (0)'

        min_bid = self.ads_context['min_bid']
        min_time_bid = self.ads_context['min_time_bid']
        max_time_bid = self.ads_context['max_time_bid']
        max_time_active = self.ads_context['max_time_active']

        accepted_token = self.ads_context['token']
        if 'bid_token' in self.op_data:
            assert self.op_data['bid_token'] == accepted_token, (
                'token not accepted as payment in community')

        if min_bid:
            assert op_bid_amount >= min_bid, (
                'bid amount (%s) is less than community minimum (%s)'
                % (op_bid_amount, min_bid))

        if min_time_bid:
            assert op_time_units >= min_time_bid, (
                'the community accepts a minimum of (%d) time units per bid; you entered (%d)'
                % (min_time_bid, op_time_units))

        if max_time_bid:
            assert op_time_units <= max_time_bid, (
                'the community accepts a maximum of (%d) time units per bid; you entered (%d)'
                % (max_time_bid, op_time_units))

        if max_time_active:
            active_units = self._get_active_time_units()
            tot_active_units = active_units + op_time_units
            assert tot_active_units <= max_time_active, (
                "total active time units (%d) will exceed community's maximum allowed (%d)"
                % (tot_active_units, max_time_active))

    def _check_ad_timeout(self):
        if self.ad_state['status'] == Status.approved:
            now = datetime.utcnow()
            start_time = self.ad_state['start_time']
            return now > start_time
        return False

    def _get_active_time_units(self):
        """Get the total number of active time units for the transacting account
            in the current Monetizer context.
           (Only for ads with status of approved(2) and scheduled(3)."""
        sql = f"""SELECT SUM(time_units) FROM ads_state
                  WHERE advertizer_id = {self.advertizer_id}
                  AND monetizer_id = {self.monetizer_id}
                  AND status > 1"""  # TODO: investigate explicit expression (2,3)
        active_units = self.db.db.select(sql)[0][0]
        return active_units or 0
