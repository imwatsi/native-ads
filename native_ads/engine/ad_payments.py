from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache
from native_ads.engine.advertizer_ops import AdvertizerOp
from native_ads.utils.validation import parse_nai_amount

class AdPayment:

    def __init__(self, db, block_num, timestamp, transfer):
        self.db = db
        self.block_num = block_num
        self.timestamp = timestamp
        self.amount, self.token = parse_nai_amount(transfer['amount'], bypass_nai_lookup=True)
        self.from_acc = transfer['from']
        self.to_acc = transfer['to']
        self.memo = transfer['memo']
        # TODO: validate memo
        self._validate_payment()
    
    def _validate_payment(self):
        ref = self.memo[4:].strip()
        assert ref.count('/') == 2, (
            "invalid ad payment memo; found (%d) '/' characters instead of 1" % ref.count)
        _values = ref.split('/')
        self.monetizer = _values[0].strip()
        ad_space = _values[1].strip()
        ad_permlink = _values[2].strip()
        # validate Monetizer account
        if self.to_acc == 'null':
            self.monetizer_id = None
        else:
            assert self.monetizer == self.to_acc, "payment sent to wrong account, check 'memo' and 'to' accounts"
            self.monetizer_id = AccountsCache.get_acc_id('monetizer', self.monetizer)
            assert self.monetizer_id is not None, (
                f"invalid payment; {self.monetizer} does not have a Monetizer account"
            )
        # validate Advertizer account
        self.advertizer_id = AccountsCache.get_acc_id('advertizer', self.from_acc)
        assert self.advertizer_id is not None, (
            f"invalid payment; {self.from_acc} does not have an Advertizer account"
        )
        # get ad_space_id
        self.ad_space_id = self.db.get_monetizer_space_id(
            self.monetizer_id, ad_space
        )
        assert self.ad_space_id is not None, (
            f"invalid payment; ad space does not exist (@{self.monetizer}/{ad_space})"
        )
        # get ad_id
        self.ad_id = AdPostsCache.get_post_id(self.from_acc, ad_permlink)
        assert self.ad_id is not None, (
            f"invalid payment; the ad post is invalid (@{self.from_acc}/{ad_permlink})"
        )
        
    def process(self):
        transfer_details = {
            'to_acc': self.to_acc,
            'monetizer_id': self.monetizer_id,
            'ad_space_id': self.ad_space_id,
            'advertizer_id': self.advertizer_id,
            'ad_id': self.ad_id,
            'amount': self.amount,
            'token': self.token
        }
        transfer_op = AdvertizerOp(
            self.db,
            self.block_num,
            self.timestamp,
            self.from_acc,
            'ad_fund',
            transfer_details
        )