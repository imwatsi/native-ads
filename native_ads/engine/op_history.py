class NativeAdOpHistory:

    # one block history of ops
    history = {'monetizer': {}, 'advertizer': {}}

    @classmethod
    def update_block_hist(cls, num, acc_type, source_acc_id, target_acc_id, action, space_id=None, ad_id=None):
        """Maintains a current block history of all valid native ad ops, to resolve conflicts."""
        details = {
            'acc_type': acc_type,
            'source_acc_id': source_acc_id,
            'target_acc_id': target_acc_id,
            'action': action,
            'space_id': space_id,
            'ad_id': ad_id
        }
        if num in cls.history[acc_type]:
            _buffer = cls.history[acc_type][num]
        else:
            if len(cls.history[acc_type]) > 0:
                # clear previous block from mem
                cls.history[acc_type].clear()
            _buffer = []

        _buffer.append(details)

        cls.history[acc_type][num] = _buffer

    @classmethod
    def check_block_hist(cls, num, acc_type, source_acc_id, target_acc_id, action, space_id=None, ad_id=None):
        """Check for a matching native ad op in block history."""
        details = {
            'acc_type': acc_type,
            'source_acc_id': source_acc_id,
            'target_acc_id': target_acc_id,
            'action': action,
            'space_id': space_id,
            'ad_id': ad_id
        }
        if num in cls.history[acc_type]:
            hist = cls.history[acc_type][num]
            for entry in hist:
                if entry == details:
                    return True
        return False