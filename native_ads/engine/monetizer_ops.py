import json

from datetime import datetime, timedelta

from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache
from native_ads.engine.op_history import NativeAdOpHistory
from native_ads.engine.op_validation import MonetizerOpValidation
from native_ads.engine.notifications import Notifications
from native_ads.utils.tools import (
    check_required_keys, check_allowed_keys,
    START_BLOCK, get_cleaned_dict
)
from native_ads.utils.defaults import Status

ALLOWED_KEYS = {
    'account_init': [
        'enabled', 'token', 'ad_types', 'burn', 'min_bid',
        'min_time_bid', 'max_time_bid', 'max_time_active',
        'scheduled_ads_delay', 'scheduled_ads_timeout'
    ],
    'create_space': [
        'ad_type', 'space_name', 'title', 'description', 'guidelines'
    ],
    'ad_approve': [
        'monetizer', 'ad_space', 'permlink', 'advertizer',
        'start_time', 'mod_notes'
    ],
    'ad_reject': [
        'monetizer', 'ad_space', 'permlink', 'advertizer',
        'mod_notes'
    ],
    'mod_add': [
        'monetizer', 'moderator', 'enabled', 'scope'
    ],
    'mod_remove': [
        'monetizer', 'moderator'
    ]
}
ALLOWED_KEYS['set_props'] = ALLOWED_KEYS['account_init']
ALLOWED_KEYS['set_space_props'] = ALLOWED_KEYS['create_space']

REQUIRED_KEYS = {
    'account_init': [],
    'set_props': [],
    'create_space': [
        'ad_type', 'space_name', 'title', 'description', 'guidelines'
    ],
    'set_space_props': [
        'space_name'
    ],
    'ad_approve': [
        'monetizer', 'ad_space', 'permlink', 'advertizer',
        'mod_notes'
    ],
    'ad_reject': [
        'monetizer', 'ad_space', 'permlink', 'advertizer',
        'mod_notes'
    ],
    'mod_add': [
        'monetizer', 'moderator'
    ],
    'mod_remove': [
        'monetizer', 'moderator'
    ]
}

OP_START = {
    'account_init': START_BLOCK,
    'set_props': START_BLOCK,
    'create_space': START_BLOCK,
    'set_space_props': START_BLOCK,
    'ad_approve': START_BLOCK,
    'ad_reject': START_BLOCK,
    'mod_add': 48000000,
    'mod_remove': 48000000
}

class MonetizerOp:
        
    def __init__(self, db, block_num, timestamp, acc, op_name, op_data):
        self.db = db
        self.block_num = block_num
        self.timestamp = timestamp
        self.acc = acc
        self.op_name = op_name
        self.op_data = op_data
        self.op_handlers = {
            'account_init': self.account_init,
            'set_props': self.set_props,
            'create_space': self.create_space,
            'set_space_props': self.set_space_props,
            'ad_approve': self.ad_approve,
            'ad_reject': self.ad_reject,
            'mod_add': self.mod_add,
            'mod_remove': self.mod_remove
        }
        try:
            if op_name not in OP_START: return
            if self.block_num < OP_START[op_name]: return
            self._validate_op_structure()
            self.process()
        except AssertionError as e:
            Notifications.new_notification(
                self.timestamp, self.acc,
                'monetizer','error', str(e)
            )

    def process(self):
        self.op_handlers[self.op_name]()
    
    def _validate_op_structure(self):
        check_required_keys(
            REQUIRED_KEYS[self.op_name],
            self.op_data.keys(),
            f'{self.op_name} op'
        )
        check_allowed_keys(
            ALLOWED_KEYS[self.op_name],
            self.op_data.keys(),
            f'{self.op_name} op'
        )
        

    def _id_prefetch_and_validation(self):
        # get advertizer ID
        self.advertizer_id = AccountsCache.get_acc_id('advertizer', self.op_data['advertizer'])
        assert self.advertizer_id is not None, (
            f"account '{self.acc}' does not have an advertizer account"
        )
        # get monetizer ID
        self.monetizer_id = AccountsCache.get_acc_id('monetizer', self.op_data['monetizer'])
        assert self.monetizer_id is not None, (
            f"account '{self.op_data['monetizer']}' does not have a monetizer account"
        )
        # get space ID
        self.space_id = self.db.get_monetizer_space_id(
            self.monetizer_id, self.op_data['ad_space']
        )
        assert self.space_id is not None, (
            f"invalid ad_space entered; could not find "
            f"(@{self.op_data['monetizer']}/{self.op_data['ad_space']}"
        )
        # get ad ID
        self.ad_id = AdPostsCache.get_post_id(self.op_data['advertizer'], self.op_data['permlink'])
        assert self.ad_id is not None, (
            f"invalid ad permlink; could not find "
            f"(@{self.acc}/{self.op_data['permlink']}"
        )
    
    def _validate_op_for_processing(self):
        """Validate the advertizer op."""

        self._validate_ad_state()

        # load Monetizer's Ads context
        self.ads_context = self.db.get_monetizer_context(self.monetizer_id)

        self._validate_ad_compliance()
        self._validate_time_ranges()
    
    def _validate_ad_state(self):
        """Checks the operation against the rules permitted for the ad's current state."""

        valid_ad = AdPostsCache.get_post_id(self.op_data['advertizer'], self.op_data['permlink'])
        assert valid_ad, 'the specified post is not a valid ad'

        self.ad_state = self.db.get_ad_state(self.ad_id, self.space_id)

        if self.ad_state:
            ad_status = self.ad_state['status']
        else:
            ad_status = None
        
        action = self.op_name

        assert self.ad_state, (
            'ad not yet submitted to Monetizer; cannot perform %s op' % action)

        if action == 'ad_approve':
            assert ad_status == Status.submitted, 'can only approve ads that are pending review'
            if self.ad_state['start_time']:
                assert 'start_time' not in self.op_data, (
                    "ad already has a start_time; cannot overwrite a customer's start_time"
                )
            else:
                assert 'start_time' in self.op_data, (
                    'no start_time provided for unscheduled ad'
                )
        elif action == 'ad_reject':
            conflict_fund = NativeAdOpHistory.check_block_hist(
                self.block_num,
                'advertizer',
                self.advertizer_id,
                self.monetizer_id,
                'ad_fund',
                self.space_id,
                self.ad_id
            )
            if not conflict_fund:
                ad_timed_out = self._check_ad_timeout()
                if not ad_timed_out:
                    assert ad_status == Status.submitted, (
                        'can only reject ads that are pending review or timed out')

    def _validate_ad_compliance(self):
        """Check if operation complies with monetizer level ad settings"""

        action = self.op_name

        if action == 'ad_approve':
            accepts_ads = self.ads_context['enabled']
            assert accepts_ads, 'Monetizer does not accept ads'
    
    def _validate_time_ranges(self):
        """Validates that time ranges for adApprove ops exist
            and are valid, i.e. no overlaps and no start_time values
            before `now() + scheduled_delay`."""

        action = self.op_name

        if action != 'ad_approve':
            return

        time_units = None
        start_time = None

        assert self.ad_state['time_units'], (
            "cannot approve an ad that doesn't have time_units specified"
        )
        time_units = self.ad_state['time_units']

        if 'start_time' in self.op_data:
            start_time = datetime.fromisoformat(self.op_data['start_time'])
        else:
            assert self.ad_state['start_time'], (
                "cannot approve an ad that doesn't have start_time specified"
            )

        sql = f"""SELECT 1 FROM ads_state
                    WHERE space_id = {self.space_id}
                    AND status > 1
                    AND tsrange(start_time, start_time
                        + (time_units * interval '1 minute'), '[]')
                    && tsrange(timestamp '{start_time}', timestamp '{start_time}'
                        + ({time_units} * interval '1 minute'), '[]')"""

        found = bool(self.db.db.select(sql))

        assert not found, 'time slot not available'

    def _check_ad_timeout(self):
        if self.ad_state['status'] == Status.approved:
            now = datetime.utcnow()
            start_time = self.ad_state['start_time']
            return now > start_time
        return False
    
    def _verify_mod_permissions(self):
        #TODO: possibly deny self-moderation
        assert self.acc ==  self.op_data['monetizer'] or self._is_allowed_mod(), (
            f"{self.acc} not authorized to moderate ads for "
            f"{self.op_data['monetizer']}'s Monetizer account"
        )
    
    def _is_allowed_mod(self):
        acc_id = AccountsCache.get_hive_acc_id(self.acc)
        assert acc_id, f"Hive account {self.acc} is not active on Native Ads"
        return self.db.is_active_moderator(acc_id, self.monetizer_id)

    def account_init(self):
        MonetizerOpValidation.acc_props(self.op_data)
        _id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert not _id, f'monetizer account for {self.acc} already initialized'
        self.db.new_monetizer_acc(self.acc, self.timestamp, self.op_data)
        AccountsCache.flag_acc('monetizer', self.acc)
        AccountsCache.check_acc_db_entry(self.acc, self.timestamp)
    
    def mod_add(self):
        assert self.op_data['monetizer'] == self.acc, 'can only set moderators for own Monetizer account'
        AccountsCache.check_acc_db_entry(self.acc, self.timestamp)
        monetizer_id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert monetizer_id is not None, f"account '{self.acc}' does not have a monetizer account"
        mod_acc_id = AccountsCache.get_hive_acc_id(self.op_data['moderator'])
        assert mod_acc_id, f"Hive account {self.op_data['moderator']} is not active on Native Ads"
        assert not self.db.is_active_moderator(mod_acc_id, monetizer_id), "moderator already active"
        details = {
            'monetizer_id': monetizer_id,
            'hive_acc_id': mod_acc_id
        }
        details['scope'] = json.dumps(self.op_data['scope']) if 'scope' in self.op_data else "{}"
        details['enabled'] = self.op_data['enabled'] if 'enabled' in self.op_data else True
        if self.db.has_moderator_entry(mod_acc_id, monetizer_id):
            self.db.update_moderator(mod_acc_id, monetizer_id, details['enabled'], details['scope'])
        else:
            self.db.add_moderator(details)
    
    def mod_remove(self):
        assert self.op_data['monetizer'] == self.acc, 'can only set moderators for own Monetizer account'
        AccountsCache.check_acc_db_entry(self.acc, self.timestamp)
        monetizer_id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert monetizer_id is not None, f"account '{self.acc}' does not have a monetizer account"
        mod_acc_id = AccountsCache.get_hive_acc_id(self.op_data['moderator'])
        assert mod_acc_id, f"Hive account {self.op_data['moderator']} is not active on Native Ads"
        assert self.db.has_moderator_entry(mod_acc_id, monetizer_id), f"account '{self.op_data['moderator']}' not a moderator"
        assert self.db.is_active_moderator(mod_acc_id, monetizer_id), "moderator already disabled"
        self.db.remove_moderator(mod_acc_id, monetizer_id)

    def set_props(self):
        MonetizerOpValidation.acc_props(self.op_data)
        acc_id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert acc_id is not None, f"account '{self.acc}' does not have a monetizer account"
        self.db.update_monetizer_acc(acc_id, self.op_data)
        AccountsCache.flag_acc('monetizer', self.acc)
    
    def create_space(self):
        MonetizerOpValidation.space_props(self.op_data)
        acc_id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert acc_id is not None, f"account '{self.acc}' does not have a monetizer account"
        details = self.op_data
        details['monetizer_id'] = acc_id
        exists = self.db.get_monetizer_space_id(acc_id, details['space_name'])
        assert exists is None, f"monetizer space_name already exists ({details['space_name']})"
        self.db.new_monetizer_space(details)
    
    def set_space_props(self):
        MonetizerOpValidation.space_props(self.op_data)
        acc_id = AccountsCache.get_acc_id('monetizer', self.acc)
        assert acc_id is not None, f"account '{self.acc}' does not have a monetizer account"
        details = self.op_data
        exists = self.db.get_monetizer_space_id(acc_id, details['space_name'])
        assert exists is not None, f"monetizer space_name does not exist ({details['space_name']})"
        del details['space_name'] # immutable
        if details == {}: return # TODO: notify
        self.db.update_monetizer_space(exists, details)
    
    def ad_approve(self):
        MonetizerOpValidation.ad_approve(self.op_data)
        self._id_prefetch_and_validation()
        self._verify_mod_permissions()
        self._validate_op_for_processing()
        details = get_cleaned_dict(
            self.op_data,
            ['monetizer', 'ad_space', 'permlink', 'advertizer']
        )
        details['status'] = Status.approved
        self.db.update_ad_state(self.ad_id, self.space_id, details)

    def ad_reject(self):
        MonetizerOpValidation.ad_reject(self.op_data)
        self._id_prefetch_and_validation()
        self._verify_mod_permissions()
        self._validate_op_for_processing()
        self.db.update_ad_state(
            self.ad_id,
            self.space_id,
            {
                'mod_notes': self.op_data['mod_notes'],
                'status': Status.draft
            }
        )