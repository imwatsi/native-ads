from native_ads.utils.defaults import NotificationCategory, NativeAdsAccountType
from native_ads.cache.accounts import AccountsCache

class Notifications:

    @classmethod
    def init(cls, db):
        cls.db = db
    
    @classmethod
    def new_notification(cls, timestamp, account, na_acc_type, category, remark):
        hive_acc = AccountsCache.get_hive_acc(account)
        if hive_acc is None:
            return
        details = {
            'created': timestamp,
            'hive_acc_id': hive_acc['id'],
            'na_acc_type': NativeAdsAccountType[na_acc_type],
            'category': NotificationCategory[category],
            'remark': remark
        }
        cls.db.new_notification(details)

    @classmethod
    def mark_read(cls, account, timestamp):
        hive_acc = AccountsCache.get_hive_acc(account)
        assert hive_acc, "account is not active on Native Ads"
        cls.db.update_notification_read(hive_acc['id'], timestamp)
        AccountsCache.update_hive_acc(account)