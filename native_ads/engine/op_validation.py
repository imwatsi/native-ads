from native_ads.utils.validation import (
    is_valid_nai, valid_start_time, valid_permlink,
    valid_bid, valid_time_units, valid_acc_name,
    valid_mod_notes)
from native_ads.utils.defaults import DEFAULT_MIN_SCHEDULED_DELAY
from native_ads.engine.ad_posts import SUPPORTED_ADS

class MonetizerOpValidation:

    @classmethod
    def acc_props(cls, params):
        if 'enabled' in params:
                assert isinstance(params['enabled'], bool), (
                    "'enabled' value must be a boolean"
                )
        if 'token' in params:
            assert isinstance(params['token'], str), (
                "'token' value must be a string"
            )
            assert is_valid_nai(params['token']), (
                "invalid NAI provided for 'token' value"
            )
        if 'ad_types' in params:
            assert isinstance(params['ad_types'], list), (
                "'ad_types' value must be an array"
            )
            for _type in params['ad_types']:
                assert _type in SUPPORTED_ADS, (
                    f"ad_types '{_type}' is not supported; supported ads: {SUPPORTED_ADS}"
                )
        if 'burn' in params:
            assert isinstance(params['burn'], bool), (
                "'burn' value must be a boolean"
            )
        if 'min_bid' in params:
            assert isinstance(params['min_bid'], float), (
                "'min_bid' value must be an floating point number"
            )
        if 'min_time_bid' in params:
            valid_time_units(params['min_time_bid'], 'acc_props')
        if 'max_time_bid' in params:
            valid_time_units(params['max_time_bid'], 'acc_props')
        if 'max_time_active' in params:
            assert valid_time_units(params['max_time_active'], 'acc_props')
        if 'scheduled_ads_delay' in params:
            delay = params['scheduled_ads_delay']
            valid_time_units(delay, 'acc_props')
            assert delay >= DEFAULT_MIN_SCHEDULED_DELAY, (
                f"'scheduled_ads_delay' value must be at least "
                f"{DEFAULT_MIN_SCHEDULED_DELAY} minutes"
            )
        if 'scheduled_ads_timeout' in params:
            valid_time_units(params['scheduled_ads_timeout'], 'acc_props')

    @classmethod
    def space_props(cls, params):
        ad_type = params['ad_type']
        title = params['title']
        description = params['description']
        guidelines = params['guidelines']
        space_name = params['space_name']
        # space name
        valid_permlink(space_name, 'space_name')
        # ad_type
        assert isinstance(ad_type, str), (
            "'ad_type' value must be a string"
        )
        assert ad_type in SUPPORTED_ADS, (
            f"ad_type '{ad_type}' is not supported; supported ads: {SUPPORTED_ADS}"
        )
        # title
        assert isinstance(title, str), (
            "'title' value must be a string"
        )
        assert len(title) < 100, (
            "length of 'title' value must be less than 100"
        )
        # description
        assert isinstance(description, str), (
            "'description' value must be a string"
        )
        assert len(description) < 1024, (
            "length of 'description' value must be less than 1024"
        )
        # guidelines
        assert isinstance(guidelines, str), (
            "'guidelines' value must be a string"
        )
    
    @classmethod
    def ad_approve(cls, params):
        valid_acc_name(params['monetizer'], 'ad_approve')
        valid_acc_name(params['advertizer'], 'ad_approve')
        valid_permlink(params['ad_space'], 'ad_approve:ad_space')
        valid_permlink(params['permlink'], 'ad_approve:permlink')
        valid_mod_notes(params['mod_notes'], 'ad_approve')
        if 'start_time' in params: valid_start_time(params['start_time'], 'ad_approve')
    
    @classmethod
    def ad_reject(cls, params):
        valid_acc_name(params['monetizer'], 'ad_reject')
        valid_acc_name(params['advertizer'], 'ad_reject')
        valid_permlink(params['ad_space'], 'ad_reject:ad_space')
        valid_permlink(params['permlink'], 'ad_reject:permlink')
        valid_mod_notes(params['mod_notes'], 'ad_reject')
    
class AdvertizerOpValidation:

    @classmethod
    def acc_props(cls, params):
        if 'profile' in params:
            assert isinstance(params['profile'], dict), (
                "'profile' value must be a JSON object"
            )

    @classmethod
    def ad_submit(cls, params):
        valid_acc_name(params['monetizer'], 'ad_submit')
        valid_permlink(params['ad_space'], 'ad_submit:ad_space')
        valid_permlink(params['permlink'], 'ad_submit:permlink')
        if 'start_time' in params: valid_start_time(params['start_time'], 'ad_submit')
        valid_time_units(params['time_units'], 'ad_submit')
        
        valid_bid(params['bid_amount'], params['bid_token'], 'ad_submit')
    
    @classmethod
    def ad_bid(cls, params):
        valid_acc_name(params['monetizer'], 'ad_bid')
        valid_permlink(params['ad_space'], 'ad_bid:ad_space')
        valid_permlink(params['permlink'], 'ad_bid:permlink')
        valid_bid(params['bid_amount'], params['bid_token'], 'ad_bid')
        if 'start_time' in params: valid_start_time(params['start_time'], 'ad_bid')
    
    @classmethod
    def ad_withdraw(cls, params):
        valid_acc_name(params['monetizer'], 'ad_bid')
        valid_permlink(params['ad_space'], 'ad_bid:ad_space')
        valid_permlink(params['permlink'], 'ad_bid:permlink')