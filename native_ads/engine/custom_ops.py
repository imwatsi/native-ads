import json
from native_ads.utils.tools import START_BLOCK
from native_ads.engine.monetizer_ops import MonetizerOp
from native_ads.engine.advertizer_ops import AdvertizerOp


ACCOUNT_TYPES = ['monetizer', 'advertizer']

class NativeAdOp:
    
    def __init__(self, db, op, block_num, timestamp):
        self.db = db
        self.op = op
        self.block_num = block_num
        self.timestamp = timestamp

    def _check_posting_auths(self, auths, req):
        assert len(auths) == req, f"{req} posting auths are needed"
        if req == 1:
            return auths[0]

    def _validate_op(self, op, block_num):
        assert isinstance(op,list), "op's json data must be in an array"
        tot_items = len(op)
        assert tot_items == 3, f"op array must contain 3 elements; found {tot_items} elements"
        op_name = op[1]
        op_data = op[2]
        assert isinstance(op_name, str), "op id must be a string"
        assert isinstance(op_data, dict), "op data must be in a JSON object"
        
    def process(self):
        acc = self._check_posting_auths(self.op['required_posting_auths'], 1)
        try:
            op_json = json.loads(self.op['json'])
        except:
            raise AssertionError ('invalid json passed')
        na_op_acc_type = op_json[0]
        if na_op_acc_type not in ACCOUNT_TYPES: return
        self._validate_op(op_json, self.block_num)
        # extract native ads data
        na_op_name = op_json[1]
        na_op_data = op_json[2]
        if na_op_acc_type == 'monetizer':
            monetizer_op = MonetizerOp(self.db, self.block_num, self.timestamp, acc, na_op_name, na_op_data)
            del  monetizer_op
        elif na_op_acc_type == 'advertizer':
            advertizer_op = AdvertizerOp(self.db, self.block_num, self.timestamp, acc, na_op_name, na_op_data)
            del advertizer_op
