"""Processes comment operations that are valid Native Ads ad posts"""

import json

from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache, AdPostUpdates
from native_ads.engine.ad_validation import AdTypeValidation
from native_ads.utils.tools import check_required_keys, check_allowed_keys, get_cleaned_dict
from native_ads.hive.server_requests import make_request
from native_ads.engine.notifications import Notifications

# TODO: move CONSTANT to sub-module when ad posts processing code is done
SUPPORTED_ADS = ['post_global']

ALLOWED_KEYS = {
    'post_global': [
        'type'
    ] # TODO: add targetting
}

REQUIRED_KEYS = {
    'post_global': [
        'type'
    ]
}

class AdPost:

    def __init__(self, db, block_num, timestamp, post_data):
        self.db = db
        self.block_num = block_num
        self.author = post_data['author']
        self.permlink = post_data['permlink']
        self.body = post_data['body']
        self.created_at = timestamp
        try:
            self.native_ads_data = self._validate_post_structure(post_data)
            self.ad_type = self.native_ads_data['type']
            self._validate_payload_structure()
            self.process()
        except AssertionError as e:
            Notifications.new_notification(
                self.created_at, self.author,
                'advertizer', 'error', str(e)
            )

    def process(self):
        # validate payload
        del self.native_ads_data['type']
        AdTypeValidation.validate_ad(self.ad_type, self.native_ads_data)
        # is advertizer acc initialized?
        advertizer_id = AccountsCache.get_acc_id('advertizer', self.author)
        assert advertizer_id, "account has not initialized an advertizer account"
        # does post exist in DB?
        post_id = AdPostsCache.get_post_id(self.author, self.permlink)
        details = {
            'created': self.created_at,
            'permlink': self.permlink,
            'ad_type': self.ad_type,
            'content': self.body,
            'properties': json.dumps(self.native_ads_data)
        }
        if not post_id:
            # new post
            details['advertizer_id'] = advertizer_id
            self.db.new_ad_post(details)
            AdPostsCache.add_new_post_to_queue(advertizer_id, self.author, details)
        else:
            # edit post
            AdPostUpdates.flag_post(post_id, self.author, self.permlink, self.ad_type)

    def _validate_post_structure(self, post_data):
        try:
            json_metadata = json.loads(post_data['json_metadata'])
        except:
            raise AssertionError (
                "invalid JSON passed in the post's 'json_metadata' field"
            )
        assert isinstance(json_metadata, dict), (
            "'json_metadata' must be a JSON object"
        )
        assert 'native-ads' in json_metadata, "missing 'native-ads' key in JSON metadata"
        native_ads_data = json_metadata['native-ads']
        assert isinstance(native_ads_data, dict), (
            "'native-ads' values must be in a JSON object"
        )
        assert 'type' in native_ads_data, (
            "missing 'type' value for ad post"
        )
        _type = native_ads_data['type']
        assert _type in SUPPORTED_ADS, (
            f"unsupported ad type provided: {_type}"
        )
        return native_ads_data

    def _validate_payload_structure(self):
        check_required_keys(
            REQUIRED_KEYS[self.ad_type],
            self.native_ads_data.keys(),
            f'{self.ad_type} ad post'
        )
        check_allowed_keys(
            ALLOWED_KEYS[self.ad_type],
            self.native_ads_data.keys(),
            f'{self.ad_type} ad post'
        )

    def _get_diff(self, old, new):
        changed_fields = {}
        for k in new:
            if old[k] != new[k]: changed_fields[k] = new[k]
        return changed_fields if len(changed_fields) > 0 else None