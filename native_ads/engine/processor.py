from native_ads.engine.custom_ops import NativeAdOp
from native_ads.engine.ad_posts import AdPost, AdPostUpdates
from native_ads.engine.monetizer_ops import MonetizerOp
from native_ads.engine.advertizer_ops import AdvertizerOp
from native_ads.engine.ad_payments import AdPayment
from native_ads.cache.accounts import AccountsCache

class BlockProcessor:

    @classmethod
    def init(cls, db):
        cls.db = db
        cls.head_block = {}
        cls.block_num = 0
        cls.block_time = ''
    
    @classmethod
    def process_block(cls, block_num, block):
        prev = block['previous']
        block_hash = block['block_id']
        timestamp = block['timestamp']

        cls.db.add_block(block_num, block_hash, prev, timestamp)
        for trans in block['transactions']:
            for op in trans['operations']:
                block_op = BlockOp(cls.db, op, block_num, timestamp)
                block_op.process()
        cls.db._save()
        cls.block_num = block_num
        cls.block_time = timestamp
        AccountsCache.flush()
        AdPostUpdates.flush(cls.db)
        cls.db._save()

class BlockOp:
    
    def __init__(self, db, operation, block_num, timestamp):
        self.db = db
        self.operation = operation
        self.block_num = block_num
        self.timestamp = timestamp
        self.op_type = self.operation['type']
        self.op_value = self.operation['value']

    def process(self):
        if self.op_type == 'custom_json_operation':
            self.process_custom_json()
        elif self.op_type == 'comment_operation':
            self.process_post()
        elif self.op_type == 'delete_comment_operation':
            pass # TODO
        elif self.op_type == 'transfer_operation':
            self.process_transfer()


    def process_custom_json(self):
        if self.op_value['id'] == 'native-ads':
            na_op = NativeAdOp(self.db, self.op_value, self.block_num, self.timestamp)
            na_op.process()

    def process_post(self):
        parent_author = self.op_value['parent_author']
        if parent_author != '': return # ignore comments
        json_metadata = self.op_value['json_metadata']
        if 'native-ads' in json_metadata:
            ad_post = AdPost(self.db, self.block_num, self.timestamp, self.op_value)
    
    def process_transfer(self):
        memo = self.op_value['memo']
        if memo[:4] == "hna:":
            ad_payment = AdPayment(self.db, self.block_num, self.timestamp, self.op_value)
            ad_payment.process()