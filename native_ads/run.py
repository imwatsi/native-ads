import os
from threading import Thread

from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache
from native_ads.database.setup import DbSetup
from native_ads.database.handlers import NativeAdsDb
from native_ads.engine.processor import BlockProcessor
from native_ads.engine.notifications import Notifications
from native_ads.hive.blocks import BlockStream
from native_ads.hive.server_requests import make_request
from native_ads.utils.tools import START_BLOCK
from native_ads.server.serve import run_server
from native_ads.config import Config


def read_block_direct(block_num):
    resp = make_request("block_api.get_block", {"block_num": block_num})
    block = resp['block']
    return block

def do_fork_check(db):
    db_head = db.get_db_head()
    if not db_head: return None
    db_block_num = db_head[0]
    db_block_hash = db_head[1]
    next_block = read_block_direct(db_block_num + 1)
    if next_block['previous'] == db_block_hash:
        print("DB fork check passed")
        return db_head
    else:
        print(f"DB fork detected. Shutting down")
        os._exit(1) # TODO: safe shut down feature

def start_sync_service(config):
    print("Sync service running")
    db = NativeAdsDb(config)
    AccountsCache.init(db)
    AdPostsCache.init(db)
    Notifications.init(db)
    db_head = do_fork_check(db)
    if not db_head:
        stream = BlockStream(START_BLOCK)
    else:
        block_num = db_head[0] + 1
        stream = BlockStream(block_num)
    # TODO: populate in-memory cache (accounts)
    BlockProcessor.init(db)

def start_server_service(config):
    run_server(config)

def run():
    print("---   Native Ads started   ---")
    config = Config().values
    DbSetup.check_db(config)
    Thread(target=start_sync_service, args=(config,)).start()
    start_server_service(config)


if __name__ == "__main__":
    run()