import json
import os

CONFIG_FIELDS = ['db_username', 'db_password', 'server_port', 'ssl_cert', 'ssl_key']

class Config:

    def __init__(self):
        self.values = {}
        self.load_config()

    def load_config(self):
        if not os.path.exists('config.dat'):
            new_conf = open('config.dat', 'w')
            new_conf.writelines(f"{field}=\n" for field in CONFIG_FIELDS)
            new_conf.close()
            print(
                'No config file detected. A blank one has been created.\n'
                'Populate it with the correct details and restart native-ads.'
            )
            os._exit(1)
        f = open('config.dat', 'r').readlines()
        for line in f:
            if '=' in line:
                setting = line.split('=')
                _key = setting[0]
                assert _key in CONFIG_FIELDS, f"invalid config key detected {_key}"
                _value = setting[1].strip('\n ')
                if '[' in _value or '{' in _value:
                    self.values[_key] = json.loads(_value)
                else:
                    self.values[_key] = _value