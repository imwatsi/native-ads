"""System-wide default values"""
from enum import IntEnum

class Status(IntEnum):
    """Labels for ad status."""
    draft = 0
    submitted = 1
    approved = 2
    scheduled = 3

class NotificationCategory(IntEnum):
    """Labels for notification categories"""
    error = 0

class NativeAdsAccountType(IntEnum):
    """Labels for native ads account types"""
    monetizer = 0
    advertizer = 1
    moderator = 2

# New accounts
MONETIZER_ACC_PROPS = {
    'enabled': False,
    'token': '@@000000021',
    'ad_types': ['post_global']
}

# Monetizer Account Properties
DEFAULT_MIN_SCHEDULED_DELAY = 1440

NATIVE_POST_TYPES = ['post_global']