import decimal
import re
from datetime import datetime

from native_ads.utils.tools import UTC_TIMESTAMP_FORMAT

NAI_MAP = {
    '@@000000013': 'HBD',
    '@@000000021': 'HIVE',
    '@@000000037': 'VESTS',
}
def is_valid_nai(token):
    """Check if a given NAI is valid"""
    # TODO: future check against registry?
    result = re.match(r'@@\d{9}', token)
    if result:
        return True
    else:
        return False

def parse_nai_amount(details, bypass_nai_lookup=True):
    raw_amount = details['amount']
    precision = details['precision']
    nai = details['nai']
    dec_amount = decimal.Decimal(raw_amount) / (10**precision)
    if not bypass_nai_lookup:
        assert nai in NAI_MAP, f"unknown NAI {nai}"
    return (dec_amount, nai)

def parsed_amount(details):
    """Returns (amount, token_name) from NAI input."""
    pass # TODO

VALID_DATE = re.compile(r'^\d\d\d\d\-\d\d-\d\dT\d\d:\d\d:\d\d$')
def valid_date(val):
    """Valid datetime (YYYY-MM-DDTHH:MM:SS)"""
    return VALID_DATE.match(val)

def valid_start_time(val, context_name):
    assert valid_date(val), (
        f"{context_name}: invalid start_time format -- provided: {val}"
    )
    now = datetime.utcnow()
    provided = datetime.strptime(val, UTC_TIMESTAMP_FORMAT)
    assert provided > now, f"{context_name}: start_time can't be in the past"


def valid_permlink(link, context_name):
    assert len(link) <= 255, f"'{context_name}': permlink must not exceed 255 characters"
    valid_chars = bool(re.match("^[A-Za-z0-9-]*$", link))
    assert valid_chars, f"invalid characters detected in permlink; only letters, numbers and dashes allowed"
    return True

def valid_bid(amount, token, context_name):
    assert isinstance(amount, float), (
        f"{context_name}:bid_amount value must be a decimal number")
    assert isinstance(token, str), (
        f"{context_name}:bid_token value must be a string"
    )
    is_nai = is_valid_nai(token)
    assert is_nai, (
        f"{context_name}:bid_token - invalid NAI format entered: {token}"
    )
    # TODO: future, possible check against register

def valid_time_units(time_units, context_name):
    assert isinstance(time_units, int), f'{context_name} time_units must be integers'
    assert time_units < 2147483647, (
        f"{context_name}time_units must be less than 2147483647")  # SQL max int

def valid_acc_name(acc, context_name):
    assert len(acc) <= 16, (
        f"{context_name}: invalid account name entered"
    )
    # TODO: other checks

def valid_mod_notes(notes, context_name):
    assert isinstance(notes, str), f"{context_name}: mod notes must be a string"
    assert len(notes) <= 500, f"{context_name}: mod_notes must not be more than 500 characters"
