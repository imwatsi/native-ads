import json

from native_ads.database.setup import DbSession
from native_ads.utils.defaults import MONETIZER_ACC_PROPS

class NativeAdsDb:
    """Avails method handlers for common DB operations and also exposes direct
       DB actions through `self.db.select`, `self.db.execute` and `self.db.execute_immediate`"""

    def __init__(self, config):
        self.db = DbSession(config)
        self.config = config
        self.schema = self.db.live_schema

    # TOOLS

    def _populate_by_schema(self, data, fields):
        result = {}
        for i in range(len(fields)):
            result[fields[i]] = data[i]
        return result


    # ACCESS METHODS

    def _select(self, table, columns=None, col_filters=None, order_by=None, limit=None):
        if columns:
            _columns = ', '.join(columns)
        else:
            _columns = "*"

        sql = f"SELECT {_columns} FROM {table}"

        if isinstance(col_filters, dict):
            _filters = []
            for col, value in col_filters.items():
                _filters.append (f"{col} = '{value}'")
            _final_filters = ' AND '.join(_filters)
            sql += f" WHERE {_final_filters}"
        elif isinstance(col_filters, str):
            sql += f" WHERE {col_filters}"
        if order_by:
            sql += f" ORDER BY {order_by}"
        if limit:
            sql += f" LIMIT {limit}"
        sql += ";"
        return self.db.select(sql)
    
    def _select_one(self, table, col_filters):
        sql = f"SELECT 1 FROM {table}"
        if isinstance(col_filters, dict):
            _filters = []
            for col, value in col_filters.items():
                _filters.append (f"{col} = '{value}'")
            _final_filters = ' AND '.join(_filters)
            sql += f" WHERE {_final_filters}"
        elif isinstance(col_filters, str):
            sql += f" WHERE {col_filters}"
        return bool(self.db.select(sql))

    def _insert(self, table, data):
        _columns = []
        _values = []
        for col, val in data.items():
            _columns.append(col)
            _values.append(f"%({col})s")
        columns = ', '.join(_columns)
        values = ', '.join(_values)
        sql = f"INSERT INTO {table} ({columns}) VALUES ({values})"
        sql += ";"
        self.db.execute(sql, data)

    def _update(self, table, data, col_filters= {}):
        _values = []
        for col, value in data.items():
            if value is None: continue
            _values.append (f"{col} = %({col})s")
        _final_values = ', '.join(_values)

        sql = f"UPDATE {table} SET {_final_values}"

        if isinstance(col_filters, dict):
            _filters = []
            for col, value in col_filters.items():
                _filters.append (f"{col} = %(f_{col})s")
                data[f"f_{col}"] = col_filters[col]
            _final_filters = ' AND '.join(_filters)
            sql += f" WHERE {_final_filters}"
        sql += ";"
        self.db.execute(sql, data)
    
    def _delete(self, table, col_filters):
        sql = f"DELETE FROM {table}"
        if isinstance(col_filters, dict):
            _filters = []
            for col, value in col_filters.items():
                _filters.append (f"{col} = %({col}s")
            _final_filters = ' AND '.join(_filters)
            sql += f" WHERE {_final_filters}"
        elif isinstance(col_filters, str):
            sql += f" WHERE {col_filters}"
        sql += ";"
        self.db.execute(sql, col_filters)

    def _save(self):
        self.db.commit()

    # GLOBAL PROPS

    def get_db_head(self):
        res = self._select('blocks', ['num', 'hash'], order_by="num DESC", limit=1)
        if res:
            return res[0]
        else:
            return None

    def get_db_version(self):
        res = self._select('global_props', ['db_version'])
        return res[0]

    def has_global_props(self):
        res = self._select('global_props')
        if res:
            return True
        else:
            return False

    # BLOCKS

    def add_block(self, block_num, block_hash, prev, created_at):
        self._insert('blocks', {
            'num': block_num,
            'hash': block_hash,
            'prev': prev,
            'created_at': created_at

        })
    
    def get_blocks_lite(self, start=None, end=None):
        """Retrieves a lite list of block numbers, filterable by start and end block numbers (inclusive)"""
        query_filter = ""

        if start and end:
            query_filter = f"num >= {start} AND num <= {end}"
        elif start:
            query_filter += f"num >= {start}"
        elif end:
            query_filter += f"num <= {end}"

        if query_filter != "":
            res = self._select('blocks', ['num'], query_filter, 'num DESC')
        else:
            res = self._select('blocks', columns=['num'], order_by='num DESC')
        return res
    
    # HIVE ACCOUNTS

    def new_hive_account(self, name, timestamp):
        details = {
            'name': name,
            'created': timestamp
        }
        self._insert('hive_accounts', details)

    def get_hive_account(self, name):
        res = self._select('hive_accounts', col_filters={'name': name})
        return self._populate_by_schema(res[0], self.schema['hive_accounts']) if res else None

    # MONETIZER ACCOUNTS

    def get_monetizer_acc(self, name):
        res = self._select('monetizer_accs', col_filters={'name': name})
        return self._populate_by_schema(res[0], self.schema['monetizer_accs']) if res else None
    
    def get_monetizer_name(self, monetizer_id):
        res = self._select('monetizer_accs', columns=['name'], col_filters={'id': monetizer_id})
        return res[0][0] if res else None

    def new_monetizer_acc(self, acc_name, timestamp, details=None):
        if not details:
            details = MONETIZER_ACC_PROPS
        else:
            for k in MONETIZER_ACC_PROPS:
                if k not in details: details[k] = MONETIZER_ACC_PROPS[k] 
        details['name'] = acc_name
        details['created'] = timestamp
        self._insert('monetizer_accs', details)
    
    def update_monetizer_acc(self, acc_id, details):
        self._update('monetizer_accs', data=details, col_filters={'id': acc_id})
    
    def new_monetizer_space(self, details):
        self._insert('monetizer_spaces', details)
    
    def update_monetizer_space(self, space_id, details):
        self._update('monetizer_spaces', details, col_filters={'id': space_id})
    
    def get_monetizer_space_id(self, acc_id, space_name):
        #  TODO: integrate into cache
        res = self._select(
            'monetizer_spaces',
            columns=['id'],
            col_filters={'monetizer_id': acc_id, 'space_name': space_name}
        )
        return res[0][0] if res else None
    
    def get_monetizer_space_ad_type(self, space_id):
        res = self._select('monetizer_spaces', columns=['ad_type'], col_filters={'id': space_id})
        return res[0][0] if res else None

    def get_monetizer_context(self, monetizer_id):
        fields = [
            'enabled', 'token', 'ad_types', 'burn', 'min_bid',
            'min_time_bid', 'max_time_bid', 'max_time_active',
            'scheduled_delay', 'scheduled_timeout'
        ]
        res = self._select(
            'monetizer_accs',
            columns=fields,
            col_filters={'id': monetizer_id}
        )
        return self._populate_by_schema(res[0], fields) if res else None



    # ADVERTIZER ACCOUNTS

    def new_advertizer_acc(self, acc_name, timestamp, details={}):
        if 'profile' in details:
            details['profile'] = json.dumps(details['profile'])
        details['name'] = acc_name
        details['created'] = timestamp
        self._insert('advertizer_accs', details)
    
    def update_advertizer_acc(self, acc_id, details):
        if 'profile' in details:
            details['profile'] = json.dumps(details['profile'])
        self._update('advertizer_accs', data=details, col_filters={'id': acc_id})

    def get_advertizer_acc(self, name):
        res = self._select('advertizer_accs', col_filters={'name': name})
        return self._populate_by_schema(res[0], self.schema['advertizer_accs']) if res else None

    def get_advertizer_name(self, advertizer_id):
        res = self._select('advertizer_accs', columns=['name'], col_filters={'id': advertizer_id})
        return res[0][0] if res else None
    
    # MODERATOR ACCOUNTS

    def is_active_moderator(self, hive_acc_id, monetizer_id):
        res = self._select('moderators', columns=['enabled'], col_filters={'hive_acc_id': hive_acc_id, 'monetizer_id': monetizer_id, 'enabled': True})
        return bool(res)
    
    def has_moderator_entry(self, hive_acc_id, monetizer_id):
        res = self._select_one('moderators', col_filters={'hive_acc_id': hive_acc_id, 'monetizer_id': monetizer_id})
        return bool(res)
    
    def add_moderator(self, details):
        self._insert('moderators', details)
    
    def update_moderator(self, hive_acc_id, monetizer_id, enabled, scope):
        self._update(
            'moderators',
            {'enabled': enabled, 'scope': scope},
            col_filters = {'hive_acc_id': hive_acc_id, 'monetizer_id': monetizer_id}
        )
    
    def remove_moderator(self, hive_acc_id, monetizer_id):
        if self.is_active_moderator(hive_acc_id, monetizer_id):
            self._update('moderators', {'enabled': False}, col_filters={'hive_acc_id': hive_acc_id, 'monetizer_id': monetizer_id})
    
    def get_moderators(self, monetizer_id):
        res = self._select('moderators', col_filters={'monetizer_id': monetizer_id})
        return self._populate_by_schema(res[0], self.schema['moderators']) if res else None
        
    # ADS

    def new_ad_post(self, details):
        self._insert('ads', details)
    
    def update_ad_post(self, ad_id, details):
        self._update('ads', details, {'id': ad_id})

    def get_ad_post(self, advertizer_id, permlink):
        res = self._select(
            'ads',
            col_filters={
                'advertizer_id': advertizer_id,
                'permlink': permlink
            }
        )
        return self._populate_by_schema(res[0], self.schema['ads']) if res else None

    def get_ad_post_by_id(self, ad_id):
        res = self._select('ads', col_filters={'id': ad_id})
        return self._populate_by_schema(res[0], self.schema['ads']) if res else None
    
    def delete_ad_post(self):
        # TODO
        pass


    # ADS STATE

    def new_ad_state(self, details):
        self._insert('ads_state', details)

    def update_ad_state(self, ad_id, space_id, details):
        self._update('ads_state', details, {'ad_id': ad_id, 'space_id': space_id})

    def get_ad_state(self, ad_id, space_id):
        fields = [
            'time_units', 'bid_amount', 'bid_token',
            'start_time', 'status'
        ]
        res = self._select(
            'ads_state',
            columns=fields,
            col_filters={
                'ad_id': ad_id,
                'space_id': space_id
            }
        )
        return self._populate_by_schema(res[0], fields) if res else None
    
    # NOTIFICATIONS

    def new_notification(self, details):
        self._insert('notifications', details)
    
    def update_notification_read(self, acc_id, timestamp):
        self._update('hive_accounts', {'notifications_read': timestamp}, {'id': acc_id})
    
    def get_notifications_last_read(self, acc_id):
        res = self._select('hive_accounts',['notifications_read'], {'id': acc_id})
        return res[0][0] if res else None