
DB_VERSION = 1

class DbSchema:
    def __init__(self):
        self.tables = {}
        self._populate_tables()

    def _populate_tables(self):
        # blocks table
        blocks = """
            CREATE TABLE IF NOT EXISTS blocks (
                num integer PRIMARY KEY,
                hash char(40) NOT NULL,
                prev char(40),
                created_at timestamp NOT NULL
            );"""
        self.tables['blocks'] = blocks

        hive_accounts = """
            CREATE TABLE IF NOT EXISTS hive_accounts (
                id serial PRIMARY KEY,
                name varchar(16) NOT NULL,
                created timestamp NOT NULL,
                notifications_read timestamp
            );"""
        self.tables['hive_accounts'] = hive_accounts

        monetizer_accs = """
            CREATE TABLE IF NOT EXISTS monetizer_accs (
                id serial PRIMARY KEY,
                name varchar(16) NOT NULL,
                created timestamp NOT NULL,
                profile text NOT NULL DEFAULT '{}',
                enabled boolean NOT NULL DEFAULT false,
                token char(11) NOT NULL DEFAULT '@@000000021',
                ad_types varchar(20) ARRAY NOT NULL,
                burn boolean NOT NULL DEFAULT false,
                min_bid numeric(10,3),
                min_time_bid integer,
                max_time_bid integer,
                max_time_active integer,
                scheduled_delay integer NOT NULL DEFAULT 1440,
                scheduled_timeout integer
            );"""
        self.tables['monetizer_accs'] = monetizer_accs

        advertizer_accs = """
            CREATE TABLE IF NOT EXISTS advertizer_accs (
                id serial PRIMARY KEY,
                name varchar(16) NOT NULL,
                created timestamp NOT NULL,
                profile text NOT NULL DEFAULT '{}'
            );"""
        self.tables['advertizer_accs'] = advertizer_accs

        monetizer_spaces = """
            CREATE TABLE IF NOT EXISTS monetizer_spaces (
                id serial PRIMARY KEY,
                monetizer_id integer NOT NULL REFERENCES monetizer_accs (id),
                space_name varchar(255) NOT NULL,
                ad_type varchar(20) NOT NULL,
                title varchar(255) NOT NULL,
                description varchar(1024) NOT NULL,
                guidelines text NOT NULL
            );"""
        self.tables['monetizer_spaces'] = monetizer_spaces

        moderators = """
            CREATE TABLE IF NOT EXISTS moderators (
                monetizer_id integer NOT NULL REFERENCES monetizer_accs (id),
                hive_acc_id integer NOT NULL REFERENCES hive_accounts (id),
                enabled boolean NOT NULL DEFAULT true,
                scope text NOT NULL DEFAULT '{}'
            );"""
        self.tables['moderators'] = moderators

        ads = """
            CREATE TABLE IF NOT EXISTS ads (
                id serial PRIMARY KEY,
                advertizer_id integer NOT NULL REFERENCES advertizer_accs(id),
                permlink varchar(255) NOT NULL,
                created timestamp NOT NULL,
                ad_type varchar(20) NOT NULL,
                content text NOT NULL,
                properties text NOT NULL,
                is_deleted boolean NOT NULL DEFAULT false
            );"""
        self.tables['ads'] = ads

        ads_state = """
            CREATE TABLE IF NOT EXISTS ads_state (
                advertizer_id integer NOT NULL REFERENCES advertizer_accs (id),
                ad_id integer NOT NULL REFERENCES ads (id),
                space_id integer NOT NULL REFERENCES monetizer_spaces (id),
                time_units integer NOT NULL,
                bid_amount numeric(10,3) NOT NULL,
                bid_token char(11) NOT NULL,
                start_time timestamp,
                status smallint NOT NULL DEFAULT 0,
                mod_notes varchar(500) DEFAULT ''
            );"""
        self.tables['ads_state'] = ads_state

        notifications = """
            CREATE TABLE IF NOT EXISTS notifications (
                id serial PRIMARY KEY,
                hive_acc_id integer NOT NULL REFERENCES hive_accounts(id),
                created timestamp NOT NULL, 
                na_acc_type smallint NOT NULL,
                category smallint NOT NULL,
                remark varchar(255) NOT NULL
            );"""
        self.tables['notifications'] = notifications

        global_props = """
            CREATE TABLE IF NOT EXISTS global_props (
                db_version smallint
            );"""
        self.tables['global_props'] = global_props
        
