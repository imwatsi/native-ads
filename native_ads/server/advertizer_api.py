"""Advertizer endpoints."""
import json

from native_ads.cache.accounts import AccountsCache
from native_ads.utils.tools import get_cleaned_dict
from native_ads.utils.defaults import Status, NativeAdsAccountType, NotificationCategory
from native_ads.server.normalize import normalize_types, populate_by_schema

async def get_account(context, name):
    acc =  AccountsCache.get_account('advertizer', name)
    if acc:
        return get_cleaned_dict(normalize_types(acc), ['id'])
    return {"error": f"'{name}' has not initialized an Advertizer account yet."}

async def get_ad_posts(context, name, status):
    db = context['db']
    valid_statuses = list(member.name for member in Status)
    assert status in valid_statuses, (
        f"'{status}' is an invalid status; valid options {valid_statuses}'"
    )
    advertizer_id = AccountsCache.get_acc_id('advertizer', name)
    assert advertizer_id is not None, f"'{name}' has not initialized an Advertizer account yet."
    sql = f""" SELECT a.permlink, a.created, a.ad_type, a.content, a.properties
                FROM ads a
                JOIN ads_state s ON s.ad_id = a.id
                                AND s.status = {Status[status]}
                                AND s.advertizer_id = {advertizer_id};"""
    res = db.db.select(sql)
    if res is None: return []
    result = []
    for p in res:
        entry = populate_by_schema(
            p,
            ['permlink', 'created', 'ad_type', 'content', 'properties']
        )
        result.append(normalize_types(entry))
    return result

async def get_account_notifications(context, name, category=None):
    db = context['db']
    if category:
        valid_categories = list(member.name for member in NotificationCategory)
        assert category in valid_categories, (
            f"'{category}' is an invalid category; valid options: {valid_categories}"
        )
    hive_acc = AccountsCache.get_hive_acc(name)
    assert hive_acc, f"'{name}' is not active on Native Ads"
    sql = f""" SELECT created, category, remark
                FROM notifications
                WHERE hive_acc_id = '{hive_acc['id']}'
                    AND na_acc_type = {NativeAdsAccountType.advertizer}"""
    if category:
        sql += f""" AND category = {NotificationCategory[category]}"""
    sql += "\nORDER BY created DESC"
    res = db.db.select(sql + ";")
    if res is None: return []
    result = []
    for n in res:
        entry = populate_by_schema(
            n,
            ['created', 'category', 'remark']
        )
        result.append(normalize_types(entry))

    return {'notifications': result, 'last_read': hive_acc['notifications_read']}