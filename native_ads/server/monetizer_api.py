"""Monetizer endpoints."""
import json

from datetime import datetime, timedelta

from native_ads.cache.accounts import AccountsCache
from native_ads.cache.ad_posts import AdPostsCache
from native_ads.utils.tools import get_cleaned_dict
from native_ads.server.normalize import normalize_types, populate_by_schema
from native_ads.utils.defaults import Status, NativeAdsAccountType, NotificationCategory

async def get_account(context, name):
    acc = AccountsCache.get_account('monetizer', name)
    if acc:
        return get_cleaned_dict(normalize_types(acc), ['id'])
    return {"error": f"'{name}' has not initialized a Monetizer account yet"}

async def get_moderators(context, name):
    db = context['db']
    monetizer_id = AccountsCache.get_acc_id('monetizer', name)
    assert monetizer_id is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f"""
            SELECT ha.name, enabled, scope
                FROM moderators m
                JOIN hive_accounts ha ON ha.id = m.hive_acc_id
                WHERE monetizer_id = {monetizer_id};"""
    res = db.db.select(sql)
    if res is None: return []
    _pre_list = []
    for r in res:
        _pre_list.append(populate_by_schema(r, ['account', 'enabled', 'scope']))
    _final_list = []
    for p in _pre_list:
        p['scope'] = json.loads(p['scope'])
        _final_list.append(p)
    return _final_list

async def get_moderation_list(context, name):
    db = context['db']
    monetizer_id = AccountsCache.get_acc_id('monetizer', name)
    assert monetizer_id is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f""" SELECT a.advertizer_id, ms.space_name, a.permlink, s.time_units,
                    s.bid_amount, s.start_time, s.mod_notes,
                    (s.bid_amount / s.time_units) AS pptu
                FROM ads a
                JOIN ads_state s ON a.id = s.ad_id
                JOIN monetizer_spaces ms ON ms.monetizer_id = {monetizer_id}
                WHERE s.status = {Status.submitted}
                ORDER BY pptu DESC;"""
    res = db.db.select(sql)
    if res is None: return []
    _pre_list = []
    for r in res:
        _pre_list.append(populate_by_schema(r,['advertizer_id', 'space_name', 'permlink', 'time_units', 'bid_amount', 'start_time', 'mod_notes', 'pptu']))
    # populate final list
    final_list = []
    for partial in _pre_list:
        _name = AccountsCache.get_account_name('advertizer', partial['advertizer_id'])
        ad_post = AdPostsCache.get_post(_name, partial['permlink'])
        ad_post['account'] = _name
        # add partial
        for k in partial:
            if k not in ad_post: ad_post[k] = partial[k]
        final_list.append(get_cleaned_dict(normalize_types(ad_post), ['id', 'advertizer_id']))
    return final_list

async def get_spaces(context, name):
    db = context['db']
    monetizer_id = AccountsCache.get_acc_id('monetizer', name)
    assert monetizer_id is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f""" SELECT space_name, ad_type, title, description,
                    guidelines
                FROM monetizer_spaces
                WHERE monetizer_id = {monetizer_id};"""
    res = db.db.select(sql)
    if res is None: return []
    space_list = []
    for space in res:
        space_list.append(
            populate_by_schema(
                space, 
                [
                    'space_name', 'ad_type', 'title',
                    'description', 'guidelines'
                ]
            )
        )
    return space_list

async def get_space_market(context, name, space_name):
    db = context['db']
    monetizer_id = AccountsCache.get_acc_id('monetizer', name)
    assert monetizer_id is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f""" SELECT a.advertizer_id, a.permlink, s.time_units,
                    s.bid_amount, s.start_time, s.mod_notes,
                    (s.bid_amount / s.time_units) AS pptu
                FROM ads a
                JOIN ads_state s ON a.id = s.ad_id
                JOIN monetizer_spaces ms ON ms.monetizer_id = {monetizer_id}
                    AND ms.space_name = '{space_name}'
                WHERE s.status = {Status.submitted}
                ORDER BY pptu DESC;"""
    res = db.db.select(sql)
    if res is None: return []
    _pre_list = []
    for r in res:
        _pre_list.append(populate_by_schema(r,['advertizer_id', 'permlink', 'time_units', 'bid_amount', 'start_time', 'mod_notes', 'pptu']))
    # populate final list
    final_list = []
    for partial in _pre_list:
        _name = AccountsCache.get_account_name('advertizer', partial['advertizer_id'])
        ad_post = AdPostsCache.get_post(_name, partial['permlink'])
        ad_post['account'] = _name
        # add partial
        for k in partial:
            if k not in ad_post: ad_post[k] = partial[k]
        final_list.append(get_cleaned_dict(normalize_types(ad_post), ['id', 'advertizer_id', 'cache_timestamp', 'cache_timestamp_accessed', 'is_deleted']))
    return final_list

async def get_account_notifications(context, name, category=None):
    db = context['db']
    valid_categories = list(member.name for member in NotificationCategory)
    if category:
        assert category in valid_categories, (
            f"invalid category entered: {category}; "
            f"valid options are {valid_categories}"
        )
    hive_acc = AccountsCache.get_hive_acc(name)
    assert hive_acc is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f""" SELECT created, category, remark
                FROM notifications
                WHERE hive_acc_id = '{hive_acc['id']}'
                    AND na_acc_type = {NativeAdsAccountType.monetizer}"""
    if category:
        sql += f""" AND category = {NotificationCategory[category]}"""
    sql += "\nORDER BY created DESC"
    res = db.db.select(sql + ";")
    if res is None: return []
    result = []
    for n in res:
        entry = populate_by_schema(
            n,
            ['created', 'category', 'remark']
        )
        result.append(normalize_types(entry))

    return {'notifications': result, 'last_read': hive_acc['notifications_read']}

async def get_live_ads(context, name, space_name):
    db = context['db']
    monetizer_id = AccountsCache.get_acc_id('monetizer', name)
    assert monetizer_id is not None, f"'{name}' has not initialized a Monetizer account yet"
    sql = f""" SELECT a.advertizer_id, a.permlink, s.time_units,
                    s.bid_amount, s.start_time, s.mod_notes,
                    (s.bid_amount / s.time_units) AS pptu
                FROM ads a
                JOIN ads_state s ON a.id = s.ad_id
                JOIN monetizer_spaces ms ON ms.monetizer_id = {monetizer_id}
                    AND ms.space_name = '{space_name}'
                WHERE s.status = {Status.scheduled}
                ORDER BY pptu DESC;"""
    res = db.db.select(sql)
    if res is None: return []
    _pre_list = []
    for r in res:
        _pre_list.append(populate_by_schema(r,['advertizer_id', 'permlink', 'time_units', 'bid_amount', 'start_time', 'mod_notes', 'pptu']))
    # populate final list
    final_list = []
    now = datetime.utcnow()
    for partial in _pre_list:
        end_time = partial['start_time'] + timedelta(minutes=partial['time_units'])
        if partial['start_time'] <= now and now < end_time:
            _name = AccountsCache.get_account_name('advertizer', partial['advertizer_id'])
            ad_post = AdPostsCache.get_post(_name, partial['permlink'])
            ad_post['account'] = _name
            # add partial
            for k in partial:
                if k not in ad_post: ad_post[k] = partial[k]
            final_list.append(get_cleaned_dict(normalize_types(ad_post), ['id', 'advertizer_id', 'cache_timestamp', 'cache_timestamp_accessed', 'is_deleted']))
    return final_list
