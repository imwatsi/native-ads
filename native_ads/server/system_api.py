"""Native Ads system endpoints."""
from native_ads.server.system_status import SystemStatus
from native_ads.server.normalize import normalize_types

async def ping(context):
    return "pong"

async def get_sync_status(context):
    return normalize_types(SystemStatus.get_sync_status())
