import ssl
from datetime import datetime
from aiohttp import web
from jsonrpcserver import method, async_dispatch as dispatch
from jsonrpcserver.methods import Methods
from native_ads.server import system_api, monetizer_api, advertizer_api
from native_ads.database.handlers import NativeAdsDb
from native_ads.server.system_status import SystemStatus
from native_ads.server.normalize import normalize_types

def build_methods():
    methods = Methods()
    methods.add(**{'sys.' + method.__name__: method for method in (
        system_api.ping,
        system_api.get_sync_status
    )})

    methods.add(**{'monetizer.' + method.__name__: method for method in (
        monetizer_api.get_account,
        monetizer_api.get_moderation_list,
        monetizer_api.get_spaces,
        monetizer_api.get_space_market,
        monetizer_api.get_account_notifications,
        monetizer_api.get_live_ads,
        monetizer_api.get_moderators,
    )})

    methods.add(**{'advertizer.' + method.__name__: method for method in (
        advertizer_api.get_account,
        advertizer_api.get_ad_posts,
        advertizer_api.get_account_notifications,
    )})

    return methods


def run_server(config):
    app = web.Application()
    app['db'] = NativeAdsDb(config)
    all_methods = build_methods()

    async def status_report(request):
        report = {
            'name': 'Native Ads (Hive)',
            'sync': normalize_types(SystemStatus.get_sync_status()),
            'timestamp': datetime.utcnow().isoformat()
        }
        return web.json_response(status=200, data=report)
    
    async def handler(request):
        request = await request.text()
        response = await dispatch(request, methods=all_methods, debug=True, context=app)
        if response.wanted:
            return web.json_response(response.deserialized(), status=response.http_status)
        else:
            return web.Response()

    app.router.add_post("/", handler)
    app.router.add_get("/", status_report)
    if config['ssl_cert'] != '' and config['ssl_key'] != '':
        context = ssl.SSLContext(protocol=ssl.PROTOCOL_TLS)
        context.load_cert_chain(
                config['ssl_cert'],
                config['ssl_key']
        )
    else:
        context = None
    web.run_app(
        app, host="0.0.0.0",
        port=config['server_port'],
        ssl_context=context
    )